# Rock, Paper, Scissors

Rock-paper-scissors is a zero-sum hand game usually played between two people, in which each player
simultaneously forms one of three shapes with an outstretched hand.
– [from Wikipedia](https://en.wikipedia.org/wiki/Rock-paper-scissors)

This repository contains a Node.JS version of the game which runs without any dependencies on your
command line.

## Requirements

Node.JS v4.3.1 or higher

## Installation

```
git clone git@bitbucket.org:sdepold/rock-paper-scissors.git
cd rock-paper-scissors
```

## Usage

The game can be started by running one of the following commands:

```
npm start
bin/rock-paper-scissors.js
```

The game can be stopped by hitting ctrl+c.
Please note that you will need a monospaced font for proper rendering!

### Options

The executable can be called with the following options:

```
--version | -v    Prints the version
--help | -h       Print the help
```

### Environment variables

The game can be configured by setting the following environment variables:

```
PLAYER_ONE_MODE [human|computer] (Default: human)     Set mode of player #1
PLAYER_ONE_NAME <String> (Default: Human)             Set name of player #1
PLAYER_TWO_MODE [human|computer] (Default: computer)  Set mode of player #2
PLAYER_TWO_NAME <String> (Default: Computer)          Set name of player #2
RENDERER [none|simple|pretty]                         Set rendering engine
```

Please note that you can theoretically play human vs human but since the second
player will see the input of the first player, it's a bit pointless. You have
been warned.

### Extended version

There is an extended version (with lizard and spock) of the game available. It's living in a feature
branch and can be run like this:

```
git checkout feature/lizard-spock
npm start
```

## Look and feel

### Simple renderer

![Simple renderer](https://s3.amazonaws.com/f.cl.ly/items/0T453D3u2i1a103s1r1A/Screen%20Recording%202016-03-13%20at%2010.11%20PM.gif?v=9df2bdbc)

### Pretty renderer

![Pretty renderer](https://s3.amazonaws.com/f.cl.ly/items/3h443N3C2M3J2L0k3N1G/Screen%20Recording%202016-03-13%20at%2010.12%20PM.gif?v=8aecd081)

## Test

In order to run the tests, you will need install some test dependency first. Running
the following commands will do that and run the tests:

```
npm install
npm test
```

## Ideas for improvements

- Make artificially delay of computer configurable via an environment variable
- Add more comprehensive statistics (# of ties, # of winning shapes, shape that won most)
- Add AI that makes choices based on opponents choices
    - If the opponent chooses e.g. paper very often it would be smart to choose scissors more often.
- Add integration tests that verify a whole game round player vs. computer
- Add graceful shutdown
    - Add stop method in game
    - Handle sigterm in executable and stop the game
    - Print goodbye message

## Project structure

- `bin/rock-paper-scissors.js`
    - Main executable file which is also exported if project is installed via npm
    - Calls `lib/rock-paper-scissors.js`
- `lib/rock-paper-scissors.js`
    - The programmatical entry point of the application
    - Takes options and passes them to the game constructor
    - Game instance is started afterwards
- `lib/game.js`
    - The component that connects the business logic with the rendering
    - Handles an endless loop internally and runs round after round
- `lib/helpers`
    - A directory with little helpers
- `lib/players`
    - A directory with player engines. Contains e.g. the human player and an artifical intelligence.
- `lib/rendereres`
    - A directory with rendering engines
- `lib/rules`
    - A directory with various rule related modules.
