'use strict';

const gulp        = require('gulp');
const jscs        = require('gulp-jscs');
const jshint      = require('gulp-jshint');
const mdBlock     = require('gulp-markdown-code-blocks');
const mocha       = require('gulp-spawn-mocha');
const runSequence = require('run-sequence');

let DEBUG = process.env.DEBUG === 'true';

gulp.task('default', function (done) {
  runSequence('lint', 'test', done);
});

gulp.task('test', function (done) {
  runSequence('test-unit', 'test-integration', done);
});

gulp.task('lint', function (done) {
  runSequence('lint-code', 'lint-readme', done);
});

gulp.task('lint-code', function () {
  return gulp
    .src([
      './gulpfile.js',
      './bin/**/*',
      './lib/**/*.js',
      './test/**/*.js'
    ])
    .pipe(jscs())
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(jshint.reporter('fail'));
});

gulp.task('lint-readme', function () {
  return gulp
    .src('./README.md')
    .pipe(mdBlock());
});

function runMochaTests (glob) {
  return function () {
    return gulp
      .src(glob, { read: false })
      .pipe(mocha({ debugBrk: DEBUG }));
  };
}

gulp.task('test-unit', runMochaTests('./test/unit/**/*-test.js'));
gulp.task('test-integration', runMochaTests('./test/integration/**/*-test.js'));
