#!/usr/bin/env node

'use strict';

const path = require('path');
const rockPaperScissors = require(path.resolve(__dirname, '..', 'lib', 'index'));
const options = parseOptions(process.argv.slice(2));
const version = require(path.resolve(__dirname, '..', 'package.json')).version;

if (options.help) {
  console.log(`
    Rock, Paper, Scissors – Help

    Usage: <ENVIRONMENT_VARIABLES> ${process.argv[1]} <options>

    Environment variables:
    PLAYER_ONE_MODE [human|computer] (Default: human)     Set mode of player #1
    PLAYER_ONE_NAME <String> (Default: Human)             Set name of player #1
    PLAYER_TWO_MODE [human|computer] (Default: computer)  Set mode of player #2
    PLAYER_TWO_NAME <String> (Default: Computer)          Set name of player #2
    RENDERER [none|simple|pretty]                         Set rendering engine

    Options:
    --help | -h     Renders the help
    --version | -v  Renders the version
  `);
  process.exit(0);
} else if (options.version) {
  console.log(`Rock, Paper, Scissors v${version}`);
  process.exit(0);
}

rockPaperScissors(options);

/////////////
// private //
/////////////

function parseOptions (args) {
  return {
    help: includes(args, '--help') || includes(args, '-h'),
    version: includes(args, '--version') || includes(args, '-v'),
    playerOneMode: process.env.PLAYER_ONE_MODE || 'human',
    playerOneName: process.env.PLAYER_ONE_NAME || 'Human',
    playerTwoMode: process.env.PLAYER_TWO_MODE || 'computer',
    playerTwoName: process.env.PLAYER_TWO_NAME || 'Computer',
    renderer: process.env.RENDERER || 'simple'
  };
}

function includes (array, needle) {
  return array.indexOf(needle) > -1;
}
