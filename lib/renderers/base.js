'use strict';

/*
The "base" renderer is meant to be the blueprint for every other renderer and defines all the
available functions.
*/
module.exports = {
  teaser: () => Promise.resolve(),
  beforeRound: () => Promise.resolve(),
  afterRound: () => Promise.resolve(),
  chooseShape: () => Promise.resolve(),
  invalidUserInput: () => Promise.resolve(),
  printChoice: () => Promise.resolve(),
  tie: () => Promise.resolve(),
  winner: () => Promise.resolve(),
  status: () => Promise.resolve()
};
