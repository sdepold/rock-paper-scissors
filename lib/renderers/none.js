'use strict';

const BaseRenderer = require('./base');

/*
The "none" renderer is a simple renderer which doesn't render anything. It can be used during
tests to omit any output while not having to mock anything.
*/
module.exports = Object.assign({}, BaseRenderer);
