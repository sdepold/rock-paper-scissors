'use strict';

const BaseRenderer = require('./base');

let teaserLines = [
// jscs:disable
// jshint ignore:start
  '',
  ' ______             _          ______                             ______                                   ',
  '(_____ \\           | |        (_____ \\                           / _____)     (_)                          ',
  ' _____) )___   ____| |  _      _____) )____ ____  _____  ____   ( (____   ____ _  ___  ___  ___   ____ ___ ',
  '|  __  // _ \\ / ___) |_/ )    |  ____(____ |  _ \\| ___ |/ ___)   \\____ \\ / ___) |/___)/___)/ _ \\ / ___)___)',
  '| |  \\ \\ |_| ( (___|  _ ( _   | |    / ___ | |_| | ____| |  _    _____) | (___| |___ |___ | |_| | |  |___ |',
  '|_|   |_\\___/ \\____)_| \\_| )  |_|    \\_____|  __/|_____)_| ( )  (______/ \\____)_(___/(___/ \\___/|_|  (___/ ',
  '                         |/                |_|             |/                                              ',
  ''
// jshint ignore:end
// jscs:enable
];

let rock = [
  '      _______',
  '  ---\'   ____)',
  '        (_____)',
  '        (_____)',
  '        (____)',
  '  ---.__(___)'
];

let paper = [
  '      _______',
  '  ---\'   ____)____',
  '            ______)',
  '            _______)',
  '           _______)',
  '  ---.__________)'
];

let scissors = [
  '      _______',
  '  ---\'   ____)____',
  '            ______)',
  '         __________)',
  '        (____)',
  '  ---.__(___)'
];

/*
The "pretty" renderer comes with some nicer text effects and shape rendering algorithms.
*/
module.exports = Object.assign({}, BaseRenderer, {
  teaser: function () {
    teaserLines.map((line) => console.log(center(line)));

    return Promise.resolve();
  },

  beforeRound: function (game) {
    let line = `Round ${game.rounds}: `;

    line += game.players.map((player) => {
      return `${player.name} (Score: ${player.score})`;
    }).join(' vs. ');

    let space = ' '.repeat(10);
    let formattedLine = `│${space}${line}${space}│`;
    let lineLength = formattedLine.length - 2;
    let borderTop = `┌${'─'.repeat(lineLength)}┐`;
    let borderBottom = `└${'─'.repeat(lineLength)}┘`;

    console.log(center(borderTop));
    console.log(center(formattedLine));
    console.log(center(borderBottom));

    return Promise.resolve();
  },

  chooseShape: function (player, shapes) {
    let lines = [
      '',
      `${player.name} (${player.type}), it\'s your turn! Choose one of the following shapes:`,
      shapes.map(formatShapeOption).join(', ')
    ];

    lines.forEach((line) => console.log(center(line)));
    process.stdout.write(center('>').replace(/\s+$/, ' '));

    return Promise.resolve();
  },

  invalidUserInput: function () {
    console.log(center('Unexpected input found. Try again.'));

    return Promise.resolve();
  },

  printChoice: function (shape) {
    console.log(shape.identifier);
    return Promise.resolve();
  },

  tie: function () {
    console.log();
    console.log(center('Tie!'));
    console.log();

    return Promise.resolve();
  },

  winner: function (shapeWinner, shapeLoser, player) {
    let claim = shapeWinner.winsAgainst[shapeLoser.name];

    renderShape(shapeWinner.name);
    console.log();
    console.log(center(`${claim}`));
    console.log(center(`${player.name} wins!`));
    console.log();

    return Promise.resolve();
  },

  status: () => Promise.resolve()
});

function renderShape (shapeName) {
  let shape = { rock, paper, scissors }[shapeName];
  let formattedLines = adjustLineLengths(shape);

  formattedLines.forEach((line) => console.log(center(line)));
}

function formatShapeOption (shape) {
  return shape.name.replace(shape.identifier, `[${shape.identifier}]`);
}

function center (s, options) {
  options = Object.assign({ left: '', right: '' }, options);

  let availableWidth = getWindowWidth();
  let padding = ~~((availableWidth - s.length - options.left.length - options.right.length) / 2);
  let paddingString = ' '.repeat(padding);

  return `${options.left}${paddingString}${s}${paddingString}${options.right}`;
}

function getWindowWidth () {
  let width = process.stdout.getWindowSize()[0];

  return (width % 2 === 0) ? width - 1 : width;
}

function adjustLineLengths (lines) {
  let length = Math.max.apply(null, lines.map((line) => line.length));

  return lines.map((line) => {
    return `${line}${' '.repeat(length - line.length)}`;
  });
}
