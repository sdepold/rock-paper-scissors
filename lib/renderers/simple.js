'use strict';

const BaseRenderer = require('./base');

/*
The "simple" renderer just prints plain text to the command line.
*/
module.exports = Object.assign({}, BaseRenderer, {
  teaser: function () {
    console.log('Welcome to Rock, Paper, Scissors!');
    console.log('');

    return Promise.resolve();
  },

  chooseShape: function (player, shapes) {
    let formattedOptions = shapes.map(formatShapeOption).join(', ');

    console.log(`${player.name}, your turn!`);
    process.stdout.write(`--> Please choose a shape (${formattedOptions}): `);

    return Promise.resolve();
  },

  invalidUserInput: function () {
    console.log('Unexpected input found. Try again.');

    return Promise.resolve();
  },

  printChoice: function (shape) {
    console.log(shape.identifier);
    return Promise.resolve();
  },

  tie: function () {
    console.log('Tie!\n');
    return Promise.resolve();
  },

  winner: function (shapeWinner, shapeLoser, player) {
    let claim = shapeWinner.winsAgainst[shapeLoser.name];
    console.log(`\n${claim}! ${player.name} wins\n`);
    return Promise.resolve();
  },

  status: function (players) {
    console.log('Game status:');
    players.forEach(function (player) {
      console.log(`- ${player.name}: ${player.score} matches won`);
    });
    console.log('');

    return Promise.resolve();
  }
});

function formatShapeOption (shape) {
  return shape.name.replace(shape.identifier, `[${shape.identifier}]`);
}
