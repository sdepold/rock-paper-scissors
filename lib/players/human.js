'use strict';

const BasePlayer = require('./base');
const stdinHelper = require('../helpers/stdin');

module.exports = class HumanPlayer extends BasePlayer {
  constructor (options) {
    super(options);
    this.name = (options || {}).name || 'Human';
    this.type = 'human';
  }

  /*
  The getShape function of the human player reads from stdin and searches
  for a shape with the respective identifier (e.g. p for paper). It throws
  an error if the user input was invalid.
  */
  getShape (shapes) {
    return stdinHelper.read().then(function (input) {
      let shape = shapes.find((shape) => shape.identifier === input.trim().toLowerCase());

      if (shape) {
        return shape;
      } else {
        throw new Error('Invalid user input!');
      }
    });
  }
};
