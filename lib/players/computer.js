'use strict';

const BasePlayer = require('./base');

module.exports = class ComputerPlayer extends BasePlayer {
  constructor (options) {
    super(options);
    this.name = (options || {}).name || 'Computer';
    this.type = 'computer';
  }

  /*
  The getShape function of the computer player randomly picks one of th shapes and
  returns the shape after an artificial delay. The promise resolution is delayed to
  imitate a normal user.
  */
  getShape (shapes) {
    let index = ~~(Math.random() * shapes.length);
    let shape = shapes[index];

    // Artificially delay the choice to imitate a normal user.
    return new Promise((resolve) => {
      setTimeout(resolve, this.getDelay(), shape);
    });
  }

  /*
  This function returns a random delay between 0 and 1s.
  TODO: It would be good idea to make this configurable via an environment variable.
  */
  getDelay () {
    return ~~(Math.random() * 1000);
  }
};
