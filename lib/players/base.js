'use strict';

module.exports = class Player {
  constructor (options) {
    this.options = Object.assign({ name: 'Player' }, options);
    this.score = 0;
    this.name = this.options.name;
  }

  won () {
    this.score++;
  }
};
