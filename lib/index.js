'use strict';

const Game = require('./game');
const requireAll = require('./helpers/require-all');
const renderers = requireAll(`${__dirname}/renderers`);

module.exports = function rockPaperScissors (options) {
  let renderer = renderers[options.renderer];
  let game = new Game(
    Object.assign(options, { renderer: renderer })
  );

  game.start();
};
