'use strict';

const fs = require('fs');

/*
  Synchronous helper function that returns all modules of a provided directory.

  Example:

  If the directory `some/dir` contains `a.js` and `b.js`, invokation and result
  looks like this:

  ```
  requireAll('some/dir')

  -->

  {
    a: function () {}, // exports from a.js
    b: function () {} // exports from b.js
  }
  ```

  The object's keys are equal to the file names (without extension).

  Options:
  - skip (Array): A list of files to be ignored.
*/
module.exports = function (dirname, _options) {
  let options = Object.assign({ skip: [] }, _options);
  let files = fs.readdirSync(dirname);

  return files.reduce(reducer(dirname, options), {});
};

function reducer (dirname, options) {
  return function (modules, file) {
    if (skipFile(file, options)) {
      return modules;
    }

    return assignModule(modules, dirname, file);
  };
}

function skipFile (file, options) {
  return options.skip.indexOf(file) > -1;
}

function assignModule (modules, dirname, file) {
  let key = file.split('.js')[0];

  return Object.assign(modules, {
    [key]: require(`${dirname}/${file}`)
  });
}
