'use strict';

module.exports = {
  /*
  This helper promisifies reading from stdin. Once called it will wait for the user to hit
  return and send the resolves the typed data. The stdin stream is paused afterwards again.

  Usage:

  stdinHelper.read().then(function (data) {
    // This will be called once the user hit return.
  });
  */
  read: function () {
    return new Promise(function (resolve) {
      process.stdin.resume();
      process.stdin.setEncoding('utf8');

      process.stdin.once('data', function (text) {
        process.stdin.pause();
        resolve(text);
      });
    });
  }
};
