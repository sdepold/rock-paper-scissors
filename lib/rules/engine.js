'use strict';

const scenarios = require('./scenarios');

/*
The rule engine can be used to compare two shapes with each other and to decide which
shape wins a fight.
*/
let ruleEngine = module.exports = {
  evaluate: function (shape1, shape2) {
    if (ruleEngine.isTie(shape1, shape2)) {
      return scenarios.TIE;
    } else if (ruleEngine.shapeWinsAgainst(shape1, shape2)) {
      return scenarios.SHAPE_ONE_WINS;
    } else {
      return scenarios.SHAPE_TWO_WINS;
    }
  },

  isTie: function (shape1, shape2) {
    return shape1.name === shape2.name;
  },

  shapeWinsAgainst: function (shape1, shape2) {
    return shape2.name in shape1.winsAgainst;
  }
};
