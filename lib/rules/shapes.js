'use strict';

/*
The following array contains the allowed shapes. Each shape defines a name, an identifier and a
`winsAgainst` object. This object contains the name of a shape and a claim which can be printed
in case of a won fight.
*/
let shapes = module.exports = [{
  name: 'paper',
  identifier: 'p',
  winsAgainst: {
    'rock': 'Paper covers rock'
  }
}, {
  name: 'rock',
  identifier: 'r',
  winsAgainst: {
    'scissors': 'Rock crushes scissors'
  }
}, {
  name: 'scissors',
  identifier: 's',
  winsAgainst: {
    'paper': 'Scissors cut paper'
  }
}];

module.exports.get = function getShape (name) {
  return shapes.find((shape) => shape.name === name);
};
