'use strict';

/*
An enum with possible scenarios.
*/
module.exports = {
  TIE: 0,
  SHAPE_ONE_WINS: 1,
  SHAPE_TWO_WINS: 2
};
