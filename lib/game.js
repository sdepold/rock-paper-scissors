'use strict';

const requireAll = require('./helpers/require-all');
const playerClasses = requireAll(`${__dirname}/players`, { skip: 'base.js' });
const ruleEngine = require('./rules/engine');
const scenarios = require('./rules/scenarios');
const shapes = require('./rules/shapes');

/*
The Game class is the main module that connects every part of the game with each other.
It contains a state and triggers rendering and business logic consecutively.

When a game is started it will run an endless loop that triggers round after round.
*/
module.exports = class Game {
  /*
  The constructor needs to be called with a configuration object.

  Configuration possibilities
  - renderer (required): A renderer module.
  - playerOneMode (required): A valid player class name.
  - playerOneName: A name for player one
  - playerTwoMode (required): A valid player class name.
  - playerTwoName: A name for player two
  */
  constructor (config) {
    validateConfiguration(config);

    this.config = config;
    this.renderer = this.config.renderer;
    this.players = [
      new playerClasses[this.config.playerOneMode]({ name: this.config.playerOneName }),
      new playerClasses[this.config.playerTwoMode]({ name: this.config.playerTwoName })
    ];
    this.rounds = 0;
  }

  /*
  This function renders the initial teaser and starts the game loop.
  */
  start () {
    return this.renderer
      .teaser(this)
      .then(() => this.loop());
  }

  /*
  This function runs a game round and calls itself afterwards.
  */
  loop () {
    return this
      .round()
      .then(() => this.loop());
  }

  /*
  This function calls the "beforeRound" renderer, gets the player shapes, evaluates the player
  shapes and calls the "afterRound" renderer.
  */
  round () {
    this.rounds++;

    return this.renderer.beforeRound(this).then(() => {
      return this.getShapes().then((shapes) => {
        return this.evaluateShapes(shapes);
      });
    }).then(() => {
      return this.renderer.afterRound(this);
    });
  }

  /*
  This function gets the shapes for player 1 and player 2.
  */
  getShapes () {
    return this
      .getShapeForPlayer(this.players[0])
      .then((shape1) => {
        return Promise.all([
          shape1,
          this.getShapeForPlayer(this.players[1])
        ]);
      });
  }

  /*
  This function calls the "chooseShape" renderer, reads the shape from the user and
  - if it was valid: returns the shape
  - if the input was invalid: calls the "invalidUserInput" renderer and calls itself
  */
  getShapeForPlayer (player) {
    return this.renderer
      .chooseShape(player, shapes)
      .then(() => player.getShape(shapes))
      .then(
        (shape) => {
          return this
            .maybePrintChosenShape(player, shape)
            .then(() => shape);
        },
        () => {
          return this.renderer
            .invalidUserInput()
            .then(() => this.getShapeForPlayer(player));
        }
      );
  }

  /*
  This function prints the choice of the player when it is a computer.
  It is done in order to imitate a normal user who would write the character
  and hit enter afterwards.
  */
  maybePrintChosenShape (player, shape) {
    if (player.type === 'computer') {
      return this.renderer.printChoice(shape);
    }

    return Promise.resolve();
  }

  /*
  This function uses the rule engine to evaluate the players' shapes.
  It handles the different rule scenarios and calls the "status" renderer afterwards.

  In case of a tie, it will call the "tie" renderer.
  Otherwise it will call the "winner" renderer with shape1, shape2 and the winner.
  */
  evaluateShapes (shapes) {
    let result = ruleEngine.evaluate.apply(ruleEngine, shapes);
    let rendering = null;

    switch (result) {
      case scenarios.TIE:
        rendering = this.renderer.tie();
        break;
      case scenarios.SHAPE_ONE_WINS:
        this.players[0].won();
        rendering = this.renderer.winner(shapes[0], shapes[1], this.players[0]);
        break;
      case scenarios.SHAPE_TWO_WINS:
        this.players[1].won();
        rendering = this.renderer.winner(shapes[1], shapes[0], this.players[1]);
        break;
      default:
        throw new Error(`Unexpected result: ${result}`);
    }

    return rendering.then(() => this.renderer.status(this.players));
  }
};

function validateConfiguration (config) {
  if (!config) {
    throw new Error('No config parameter passed');
  }

  if (!config.renderer) {
    throw new Error('Renderer is not configured');
  }

  if (!config.playerOneMode || !config.playerTwoMode) {
    throw new Error('Player modes are not configure');
  }

  [config.playerOneMode, config.playerTwoMode].forEach((playerMode) => {
    if (!playerMode) {
      throw new Error('Player modes are not configure');
    }

    if (!(playerMode in playerClasses)) {
      throw new Error(`Unknown player mode detected: ${playerMode}`);
    }
  });
}
