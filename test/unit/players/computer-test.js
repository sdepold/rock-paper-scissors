'use strict';

const ComputerPlayer = require('../../../lib/players/computer');

describe('players/computer', () => {
  let player;

  beforeEach(() => {
    player = new ComputerPlayer();
    player.getDelay = () => 0; // Overwrite the computer player's delay with 0.
  });

  describe('constructor', () => {
    it('sets a name', () => {
      expect(player.name).to.eql('Computer');
    });

    it('sets a type', () => {
      expect(player.type).to.eql('computer');
    });
  });

  describe('getShape', () => {
    it('resolves to a random shape', () => {
      return player.getShape([1]).then((shape) => {
        expect(shape).to.eql(1);
      });
    });
  });
});
