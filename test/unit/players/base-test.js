'use strict';

const BasePlayer = require('../../../lib/players/base');

describe('players/base', () => {
  let player;

  beforeEach(() => {
    player = new BasePlayer();
  });

  describe('constructor', () => {
    it('assigns options', () => {
      expect(player.options).to.eql({ name: 'Player' });
    });

    it('sets an initial score', () => {
      expect(player.score).to.eql(0);
    });

    it('sets a name', () => {
      expect(player.name).to.eql('Player');
    });

    it('sets a custom name', () => {
      player = new BasePlayer({ name: 'Han Solo' });
      expect(player.name).to.eql('Han Solo');
    });
  });

  describe('won', () => {
    it('increases the score by one', () => {
      player.won();
      expect(player.score).to.eql(1);
    });
  });
});
