'use strict';

const HumanPlayer = require('../../../lib/players/human');

describe('players/human', () => {
  let player;

  beforeEach(() => {
    player = new HumanPlayer();
  });

  describe('constructor', () => {
    it('sets a name', () => {
      expect(player.name).to.eql('Human');
    });

    it('sets a type', () => {
      expect(player.type).to.eql('human');
    });
  });

  describe('getShape', () => {
    it('returns the shape that matches the input', () => {
      setTimeout(() => {
        process.stdin.emit('data', 'a');
      }, 100);

      let shape = { identifier: 'a' };

      return player.getShape([shape]).then((_shape) => {
        expect(_shape).to.eql(shape);
      });
    });

    it('is case insensitive', () => {
      setTimeout(() => {
        process.stdin.emit('data', 'A');
      }, 100);

      let shape = { identifier: 'a' };

      return player.getShape([shape]).then((_shape) => {
        expect(_shape).to.eql(shape);
      });
    });

    it('rejects when the input does not match any shape', () => {
      setTimeout(() => {
        process.stdin.emit('data', 'b');
      }, 100);

      let shape = { identifier: 'a' };

      return player.getShape([shape]).catch((err) => {
        expect(err.message).to.eql('Invalid user input!');
      });
    });
  });
});
