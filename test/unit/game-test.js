'use strict';

const Game = require('../../lib/game');
const noneRenderer = require('../../lib/renderers/none');
const sinon = require('sinon');

require('sinon-as-promised')(Promise);

describe('game', () => {
  describe('constructor', () => {
    it('throws an error if configuration is not passed', () => {
      expect(() => new Game()).to.throw('No config parameter passed');
    });

    it('throws an error if renderer is not configured', () => {
      expect(() => new Game({})).to.throw('Renderer is not configured');
    });

    it('throws an error if player modes are not configured', () => {
      expect(() => {
        return new Game({ renderer: noneRenderer });
      }).to.throw('Player modes are not configure');
    });

    it('throws an error if configured player modes are unknown', () => {
      expect(() => {
        return new Game({
          renderer: noneRenderer,
          playerOneMode: 'unknown',
          playerTwoMode: 'unknown'
        });
      }).to.throw('Unknown player mode detected: unknown');
    });

    describe('with minimal configuration', () => {
      let game;

      beforeEach(() => {
        game = new Game({
          renderer: noneRenderer,
          playerOneMode: 'human',
          playerTwoMode: 'computer'
        });
      });

      it('stores the configuration', () => {
        expect(game.config).to.eql({
          renderer: noneRenderer,
          playerOneMode: 'human',
          playerTwoMode: 'computer'
        });
      });

      it('assigns default player names', () => {
        expect(game.players[0].name).to.eql('Human');
        expect(game.players[1].name).to.eql('Computer');
      });
    });
  });

  describe('life cycle', () => {
    let game, mocks;

    beforeEach(() => {
      game = new Game({
        renderer: noneRenderer,
        playerOneMode: 'human',
        playerTwoMode: 'computer'
      });
      mocks = [];
    });

    afterEach(() => {
      mocks.forEach((mock) => mock.restore());
    });

    describe('start', () => {
      it('renders the teaser', () => {
        let rendererMock = sinon.mock(noneRenderer);

        mocks.push(rendererMock);
        game.loop = () => Promise.resolve();
        rendererMock.expects('teaser').once().resolves();

        return game.start().then(() => rendererMock.verify());
      });

      it('initializes the game loop', () => {
        let gameMock = sinon.mock(game);

        mocks.push(gameMock);
        gameMock.expects('loop').once().resolves();

        return game.start().then(() => gameMock.verify());
      });
    });
  });
});
