'use strict';

const ruleEngine = require('../../../lib/rules/engine');
const scenarios = require('../../../lib/rules/scenarios');
const shapes = require('../../../lib/rules/shapes');

describe('rules/engine', () => {
  let rock = shapes.get('rock');
  let paper = shapes.get('paper');

  describe('evaluate', () => {
    it('detects tie if both shapes are equal', () => {
      expect(ruleEngine.evaluate(rock, rock)).to.eql(scenarios.TIE);
    });

    it('can detect shape one as winner', () => {
      expect(ruleEngine.evaluate(paper, rock)).to.eql(scenarios.SHAPE_ONE_WINS);
    });

    it('can detect shape two as winner', () => {
      expect(ruleEngine.evaluate(rock, paper)).to.eql(scenarios.SHAPE_TWO_WINS);
    });
  });

  describe('isTie', () => {
    it('returns true if both shapes are equal', () => {
      expect(ruleEngine.isTie(rock, rock)).to.be.true();
    });

    it('returns false if the shapes differ', () => {
      expect(ruleEngine.isTie(rock, paper)).to.be.false();
    });
  });

  describe('shapeWinsAgainst', () => {
    it('returns true if shape1 wins against shape2', () => {
      expect(ruleEngine.shapeWinsAgainst(paper, rock)).to.be.true();
    });

    it('returns false if shape1 does not win against shape2', () => {
      expect(ruleEngine.shapeWinsAgainst(rock, paper)).to.be.false();
    });
  });
});
