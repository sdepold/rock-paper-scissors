'use strict';

const shapes = require('../../../lib/rules/shapes');

describe('rules/shapes', () => {
  describe('format', () => {
    it('declares the same structure for every shape', () => {
      shapes.forEach((shape) => {
        expect(shape).to.have.all.keys(['name', 'identifier', 'winsAgainst']);
      });
    });
  });

  describe('get', () => {
    it('returns the shape if it exists', () => {
      expect(shapes.get('rock')).to.be.an('object');
    });

    it('returns undefined if the shape does not exist', () => {
      expect(shapes.get('unknown')).to.be.undefined();
    });
  });
});
