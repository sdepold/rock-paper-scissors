'use strict';

const scenarios = require('../../../lib/rules/scenarios');

describe('rules/scenarios', () => {
  it('declares scenarios', () => {
    expect(scenarios).to.have.all.keys(['TIE', 'SHAPE_ONE_WINS', 'SHAPE_TWO_WINS']);
  });
});
