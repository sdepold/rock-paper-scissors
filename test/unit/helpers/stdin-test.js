'use strict';

const stdinHelper = require('../../../lib/helpers/stdin');

describe('helpers/stdin', () => {
  describe('read', () => {
    it('returns the data from stdin', () => {
      setTimeout(() => {
        process.stdin.emit('data', 'test');
      }, 100);

      return stdinHelper.read().then((data) => {
        expect(data).to.eql('test');
      });
    });

    describe('stdin state', () => {
      it('is paused initially', () => {
        expect(process.stdin.isPaused()).to.be.true();
      });

      it('is resumed during read', () => {
        setTimeout(() => {
          expect(process.stdin.isPaused()).to.be.false();
          process.stdin.emit('data', 'test');
        }, 100);

        return stdinHelper.read();
      });

      it('is paused after read', () => {
        setTimeout(() => {
          process.stdin.emit('data', 'test');
        }, 100);

        return stdinHelper.read().then(() => {
          expect(process.stdin.isPaused()).to.be.true();
        });
      });
    });
  });
});
