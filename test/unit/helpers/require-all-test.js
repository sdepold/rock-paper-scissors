'use strict';

const requireAll = require('../../../lib/helpers/require-all');
const fixtureDir = `${__dirname}/../fixtures/helpers/require-all`;

describe('helpers/require-all', () => {
  it('returns an object', () => {
    expect(requireAll(fixtureDir)).to.be.an('object');
  });

  it('respects all modules by default', () => {
    expect(
      requireAll(fixtureDir)
    ).to.eql(
      { a: 'a', b: 'b', c: 'c' }
    );
  });

  it('optionally skips modules', () => {
    expect(
      requireAll(fixtureDir, { skip: ['a.js', 'b.js'] })
    ).to.eql(
      { c: 'c' }
    );
  });
});
