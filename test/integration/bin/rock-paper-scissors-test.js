'use strict';

const execSync = require('child_process').execSync;
const path = require('path');
const executablePath = path.resolve(__dirname, '..', '..', '..', 'bin', 'rock-paper-scissors.js');

describe('bin/rock-paper-scissors', function () {
  it('prints the version', () => {
    let result = execSync(`${executablePath} -v`);
    let output = result.toString().trim();
    let version = require('../../../package.json').version;

    expect(output).to.eql(`Rock, Paper, Scissors v${version}`);
  });

  it('prints the help', () => {
    let result = execSync(`${executablePath} -h`);
    let output = result.toString().trim();

    expect(output).to.include('Rock, Paper, Scissors – Help');
  });
});
