var chai = require('chai');
var dirtyChai = require('dirty-chai');

chai.use(dirtyChai);

// Set up globals for testing DSL
global.expect = chai.expect;
